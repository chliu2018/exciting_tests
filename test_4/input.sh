#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kentmwz@gmail.com
#SBATCH -D .
#SBATCH -p main
#SBATCH -n 8
#SBATCH -t 2:00:00
#SBATCH -A snic2021-3-34
#SBATCH -J input
#SBATCH -c 1
source /cfs/klemming/projects/snic/xsolas/chliu/bash_files/load_ase-exciting_oxygen_202209.bash
export OMP_NUM_THREADS=1
srun exciting_debug_mpismp

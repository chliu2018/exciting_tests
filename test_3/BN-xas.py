import os
#from ase import Atoms
from ase.build import bulk
from ase.calculators.exciting import Exciting
from ase.io import write,read
from ase.units import Bohr, Ha
import numpy as np

name = 'BN'
xc = 'GGA_PBE_SOL'
task ='XAS-full'
proj = '%s_%s_%s'%(name,xc,task)
ngridk = '4 4 4'
ngridq = '2 2 2'
autormt = 'true'

a = 6.83136 * Bohr
atoms = bulk(name,crystalstructure='zincblende',a=a)
write('geo-%s.traj'%proj,atoms)

paramdict = {
    'title':{'text()':proj},
    'groundstate':{
        'ngridk':ngridk,
        'xctype':xc,
        'gmaxvr':'14.0',
    },
    'xs':{
        'xstype':'BSE',
        'ngridk':ngridq,
        'vkloff':'0.05 0.15 0.25',
        'ngridq':ngridq,
        'nempty':'30',
        'gqmax':'3.0',
        'broad':'0.018',
        'tevout':'true',
        'energywindow':{
            'intv':'13.4 16.5',
            'points':'1500',
        },
        'screening':{
            'screentype':'full',
            'nempty':'100'
        },
        'BSE':{
            'xas':'true',
            'xasspecies':'2',
            'xasatom':'1',
            'xasedge':'K',
            'bsetype':'singlet',
            'nstlxas':'1 15',#first 15 unoccupied states
            'coupling':'true'
        },
        'qpointset':{
            'qpoint':{'text()':'0.0 0.0 0.0'}
        },
    },
}

calc = Exciting(
    mpi_command = 'mpprun',
    bin = 'exciting_mpismp',
    dir = 'BSE-XAS',
    speciespath=os.environ['EXCITINGROOT'] + '/species',
    autormt = autormt,
    paramdict= paramdict
)


atoms.set_calculator(calc)

e_fin = atoms.get_potential_energy()

with open('Energy_%s.txt'%proj,'w') as f:
    f.write('%.2f\n'%e_fin)

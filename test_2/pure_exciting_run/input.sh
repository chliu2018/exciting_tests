#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kentmwz@gmail.com
#SBATCH -D .
#SBATCH -p main
#SBATCH -n 1
#SBATCH -t 5:00:00
#SBATCH -A snic2021-3-34
#SBATCH -J input
#SBATCH -c 4
source /cfs/klemming/projects/snic/xsolas/chliu/bash_files/load_ase-exciting_oxygen_202209.bash
srun exciting_debug_mpismp

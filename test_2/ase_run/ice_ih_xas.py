import os
from ase.calculators.exciting import Exciting
from ase.io import write,read
from ase.units import Bohr, Ha
from ase.calculators.calculator import kptdensity2monkhorstpack as k2m
import numpy as np

name = 'ice-ih'
do = 'fromscratch'
mpi_command = 'srun'
xc = 'LDA_PW'
task = 'XAS'
kpts_d = 2.0
nf = 1#scaling factor of the number of unoccupied state
nempty = str(30*nf)
nempty_s = str(100*nf)
nstates = str(15*nf)
autormt = True
tshift = False

qpts_d = kpts_d/2.
proj = '%s_%s_%s'%(name,xc,task)
atoms = read('../geo/%s.traj'%name)
symlist = atoms.get_chemical_symbols()
symlist = np.array(symlist)
#atoms.new_array('rmt',np.where(symlist == 'O',1.0,0.7)*Bohr)

kpts = k2m(atoms,kpts_d,even=False)
qpts = k2m(atoms,qpts_d,even=False)

paramdict = {
    'title':{'text()':proj},
    'groundstate':{
        'do':do,
        'ngridk':'%d %d %d'%(kpts[0],kpts[1],kpts[2]),
        'xctype':xc,
        'gmaxvr':'16.0',
        'rgkmax':'3.0',
    },
    'xs':{
        'xstype':'BSE',
        'ngridk':'%d %d %d'%(qpts[0],qpts[1],qpts[2]),
        'vkloff':'0.05 0.15 0.25', 
        'ngridq':'%d %d %d'%(qpts[0],qpts[1],qpts[2]),
        'nempty':nempty,
        'gqmax':'3.0',
        'broad':'0.018',
        'tevout':'true',
        'energywindow':{
            'intv':'17.8 20.7',
            'points':'1500',
        },
        'screening':{
            'screentype':'full',
            'nempty':nempty_s,
        },
        'BSE':{
            'xas':'true',
            'xasspecies':'1',
            'xasatom':'1',
            'xasedge':'K',
            'bsetype':'singlet',
            'nstlxas':'1 %s'%nstates,#first several unoccupied states
            #'coupling':'true'
        },
        'qpointset':{
            'qpoint':{'text()':'0.0 0.0 0.0'}
        },
    },
}

calc = Exciting(
    mpi_command = mpi_command,
    bin = 'exciting_debug_mpismp',
    dir = proj,
    speciespath=os.environ['EXCITINGROOT'] + '/species',
    autormt = autormt,
    tshift = tshift,
    paramdict= paramdict
)
    
    
atoms.set_calculator(calc)
    
e_fin = atoms.get_potential_energy()
    
with open('Energy_%s.txt'%proj,'w') as f:
    f.write('%.2f\n'%e_fin)

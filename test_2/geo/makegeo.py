from ase.io import read, write
from ase.build import niggli_reduce

atoms = read('ice-ih.traj')
niggli_reduce(atoms)
write('ice-ih_min.traj',atoms)

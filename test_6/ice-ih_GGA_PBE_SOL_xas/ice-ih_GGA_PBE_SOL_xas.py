import os
from ase.calculators.exciting import Exciting
from ase.io import write,read
from ase.units import Bohr, Ha
from ase.calculators.calculator import kptdensity2monkhorstpack as k2m
import numpy as np

name = 'ice-ih'
do = 'fromscratch'
mpi_command = 'mpprun'
xc = 'GGA_PBE_SOL'
task = 'xas'
kpts_d = 3.5
qpts_d = 2.5
rgkmax = '4.0'
autormt = True
tshift = False
proj = 'ice-ih_GGA_PBE_SOL_xas'
atoms = read('../geo/%s.traj'%name)
nf = sum([atom.symbol == 'O' for atom in atoms])#scaling factor of the number of unoccupied state
nempty, nempty_scr, nstates = str(30*nf), str(100*nf), str(15*nf) 

kpts = k2m(atoms,kpts_d,even=False)
qpts = k2m(atoms,qpts_d,even=False)

paramdict = {
    'title':{'text()':proj},
    'groundstate':{
    'do':do,
    'ngridk':'%d %d %d'%(kpts[0],kpts[1],kpts[2]),
    'xctype':xc,
    'gmaxvr':'16.0',
    'rgkmax':rgkmax,
    },
    'gw':{
        'taskname':'g0w0',
        'ngridq':'%d %d %d'%(kpts[0],kpts[1],kpts[2]),
        'nempty':nempty,
        'ibgw':'1',
        'nbgw':nstates,
        'coreflag':'xal',
        'mixbasis':{
            'lmaxmb':'3',
            'epsmb':'1.d-4',
            'gmb':'1.0'
        },
        'freqgrid':{'nomeg':'12'},
    },
    'xs':{
        'xstype':'BSE',
        'ngridk':'%d %d %d'%(qpts[0],qpts[1],qpts[2]),
        'vkloff':'0.05 0.15 0.25', 
        'ngridq':'%d %d %d'%(qpts[0],qpts[1],qpts[2]),
        'nempty':nempty,
        'gqmax':'3.0',
        'broad':'0.018',
        'tevout':'true',
        'energywindow':{
            'intv':'17.8 20.7',
            'points':'1500',
        },
        'screening':{
            'screentype':'full',
            'nempty':nempty_scr,
        },
        'BSE':{
            'xas':'true',
            'xasspecies':'1',
            'xasatom':'1',
            'xasedge':'K',
            'bsetype':'singlet',
            'nstlxas':'1 %s'%nstates,#first several unoccupied states
            #'coupling':'true'
        },
        'qpointset':{
            'qpoint':{'text()':'0.0 0.0 0.0'}
        },
    },
}

calc = Exciting(
    mpi_command = mpi_command,
    bin = 'exciting_mpismp',
    dir = proj,
    speciespath=os.environ['EXCITINGROOT'] + '/species',
    autormt = autormt,
    tshift = tshift,
    paramdict= paramdict
)
    
    
atoms.set_calculator(calc)
            
e_fin = atoms.get_potential_energy()
                
with open('Energy_%s.txt'%proj,'w') as f:
    f.write('%.2f\n'%e_fin)

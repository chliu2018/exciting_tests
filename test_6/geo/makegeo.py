from ase.io import read, write
from ase.build import niggli_reduce
from ase.build.tools import sort
import numpy as np

if 0:
    atoms = read('ice-ih.traj')
    niggli_reduce(atoms)
    write('ice-ih_min.traj',atoms)

if 0:
    atoms = read('ice-vii.cif')
    del_list = [4,5,7,8]
    mask = np.array([i not in del_list for i in range(len(atoms))])
    atoms = atoms[mask]
    write('ice-vii.traj',atoms)

if 0:
    atoms = read('ice-vii.cif')
    del_list = [4,5,2,8]
    mask = np.array([i not in del_list for i in range(len(atoms))])
    atoms = atoms[mask]
    write('ice-vii_1.traj',atoms)

if 0:
    atoms = read('ice-vii.cif')
    del_list = [4,5,2,9]
    mask = np.array([i not in del_list for i in range(len(atoms))])
    atoms = atoms[mask]
    write('ice-vii_2.traj',atoms)
    
if 1:
    atoms = read('h2o_32.xyz')
    atoms.center(vacuum=1.)
    atoms = sort(atoms,tags = 1-atoms.get_atomic_numbers())
    print(atoms.get_chemical_symbols())
    write('h2o_32.traj',atoms)

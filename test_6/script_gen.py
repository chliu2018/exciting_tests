import os

if os.path.isdir('/cfs/klemming/projects/snic/xsolas/chliu/'):
    exciting_sub = '/cfs/klemming/projects/snic/xsolas/chliu/submission_scripts/ase-exciting_oxygen_sub.202209.py'
    mpi_command = 'srun'
elif os.path.isdir('/nfs/home/chliu/'):
    exciting_sub = '/nfs/home/chliu/submission_scripts/ase-exciting_oxygen_sub.202209.py'
    mpi_command = 'mpirun'
elif os.path.isdir('/proj/xsolas/users/x_chliu/'):
    exciting_sub = '/proj/xsolas/users/x_chliu/submission_scripts/ase-exciting_oxygen_sub.202209.py'
    mpi_command = 'mpprun'

names = ['ice-ih','h2o_32']
task = 'xas'
qpts_d = 2.5
xcs = ['GGA_PBE']
ncpu = [32*3,32*4]

#note: nstates = 15*nf for xas but 5*nf for xes

with open('batch_sub.sh','w') as f:
    f.write('es=%s\n'%exciting_sub)

for i,name in enumerate(names):
    for xc in xcs:
        proj = '%s_%s_%s'%(name,xc,task)
        if not os.path.isdir(proj):
            os.system('mkdir %s'%proj)
        if not os.path.isfile('%s/Energy_%s.txt'%(proj,proj)):
            with open('%s/%s.py'%(proj,proj),'w') as f:
                f.write("""\
import os
from ase.calculators.exciting import Exciting
from ase.io import write,read
from ase.units import Bohr, Ha
from ase.calculators.calculator import kptdensity2monkhorstpack as k2m
import numpy as np

name = '%s'
do = 'fromscratch'
mpi_command = '%s'
xc = '%s'
task = '%s'
kpts_d = 3.5
qpts_d = %.1f
rgkmax = '4.0'
autormt = True
tshift = False
proj = '%s'
"""%(name,mpi_command,xc,task,qpts_d,proj))
                f.write("""\
atoms = read('../geo/%s.traj'%name)
nf = sum([atom.symbol == 'O' for atom in atoms])#scaling factor of the number of unoccupied state
nempty, nempty_scr, nstates = str(30*nf), str(100*nf), str(15*nf) 

kpts = k2m(atoms,kpts_d,even=False)
qpts = k2m(atoms,qpts_d,even=False)

paramdict = {
    'title':{'text()':proj},
    'groundstate':{
    'do':do,
    'ngridk':'%d %d %d'%(kpts[0],kpts[1],kpts[2]),
    'xctype':xc,
    'gmaxvr':'16.0',
    'rgkmax':rgkmax,
    },
    'gw':{
        'taskname':'g0w0',
        'ngridq':'%d %d %d'%(kpts[0],kpts[1],kpts[2]),
        'nempty':nempty,
        'ibgw':'1',
        'nbgw':nstates,
        'coreflag':'xal',
        'mixbasis':{
            'lmaxmb':'3',
            'epsmb':'1.d-4',
            'gmb':'1.0'
        },
        'freqgrid':{'nomeg':'12'},
    },
    'xs':{
        'xstype':'BSE',
        'ngridk':'%d %d %d'%(qpts[0],qpts[1],qpts[2]),
        'vkloff':'0.05 0.15 0.25', 
        'ngridq':'%d %d %d'%(qpts[0],qpts[1],qpts[2]),
        'nempty':nempty,
        'gqmax':'3.0',
        'broad':'0.018',
        'tevout':'true',
        'energywindow':{
            'intv':'17.8 20.7',
            'points':'1500',
        },
        'screening':{
            'screentype':'full',
            'nempty':nempty_scr,
        },
        'BSE':{
            'xas':'true',
            'xasspecies':'1',
            'xasatom':'1',
            'xasedge':'K',
            'bsetype':'singlet',
            'nstlxas':'1 %s'%nstates,#first several unoccupied states
            #'coupling':'true'
        },
        'qpointset':{
            'qpoint':{'text()':'0.0 0.0 0.0'}
        },
    },
}

calc = Exciting(
    mpi_command = mpi_command,
    bin = 'exciting_mpismp',
    dir = proj,
    speciespath=os.environ['EXCITINGROOT'] + '/species',
    autormt = autormt,
    tshift = tshift,
    paramdict= paramdict
)
    
    
atoms.set_calculator(calc)
            
e_fin = atoms.get_potential_energy()
                
with open('Energy_%s.txt'%proj,'w') as f:
    f.write('%.2f\\n'%e_fin)
""")
            with open('batch_sub.sh','a') as f:
                            f.write("""\
cd %s
${es} -i %s.py -p %d -t 20:00 -C fat\ --exclusive
cd ..
"""%(proj,proj,ncpu[i]))

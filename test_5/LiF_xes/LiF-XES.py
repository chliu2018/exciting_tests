import os
#from ase import Atoms
from ase.build import bulk
from ase.calculators.exciting import Exciting
from ase.io import write,read
from ase.units import Bohr, Ha
import numpy as np

name = 'LiF'
xc = 'GGA_PBE_SOL'
task ='XES'
proj = '%s_%s_%s'%(name,xc,task)
ngridk = '10 10 10'
ngridq = '4 4 4'
autormt = 'false'

a = 7.608 * Bohr
atoms = bulk(name,crystalstructure='rocksalt',a=a)
write('geo-%s.traj'%proj,atoms)

paramdict = {
    'title':{'text()':proj},
    'groundstate':{
        'do':'fromscratch',
        'ngridk':ngridk,
        'xctype':xc,
        'gmaxvr':'14.0'
    },
    'xs':{
        'xstype':'BSE',
        'ngridk':ngridq,
        'vkloff':'0.097 0.273 0.493',
        'ngridq':ngridq,
        'nempty':'30',
        'gqmax':'3.0',
        'broad':'0.007',
        'scissor':'0.20947',
        'tevout':'true',
        'energywindow':{
            'intv':'23.9 24.1',
            'points':'1200',
        },
        'screening':{
            'screentype':'full',
            'nempty':'100'
        },
        'BSE':{
            'xes':'true',
            'bsetype':'IP',
            'xasspecies':'2',
            'xasatom':'1',
            'nstlxas':'1 5',
        },
        'qpointset':{
            'qpoint':{'text()':'0.0 0.0 0.0'}
        },
    },
}

calc = Exciting(
    mpi_command = 'mpprun',
    bin = 'exciting_debug_mpismp',
    dir = 'BSE-XES',
    speciespath=os.environ['EXCITINGROOT'] + '/species',
    autormt = autormt,
    paramdict= paramdict
)


atoms.set_calculator(calc)

e_fin = atoms.get_potential_energy()

with open('Energy_%s.txt'%proj,'w') as f:
    f.write('%.2f\n'%e_fin)

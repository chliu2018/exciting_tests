
================================================================================
=                            GW input parameters                               =
================================================================================


 GW taskname:

   g0w0 - G0W0 run

--------------------------------------------------------------------------------

 Frequency integration parameters:
 Number of frequencies:           12
 Cutoff frequency:    1.0000000000000000     
 Grid type:
   gauleg2 - Double Gauss-Legendre grid: [0, freqmax] + [freqmax, infty]
 Convolution method:
   imfreq : weights calculated for imaginary frequecies

--------------------------------------------------------------------------------

 Correlation self-energy parameters:
 Solution of the QP equation:
   0 - perturbative solution
 Energy alignment:
   0 - no alignment
 Analytic continuation method:
  PADE - Thiele's reciprocal difference method (by H. J. Vidberg and J. W. Serence, J. Low Temp. Phys. 29, 179 (1977))
 Scheme to treat singularities:
  Auxiliary function method by S. Massidda, M. Posternak, and A. Baldereschi, PRB 48, 5058 (1993)

--------------------------------------------------------------------------------

 Mixed product basis parameters:
   MT part:
     Angular momentum cutoff:            3
     Linear dependence tolerance factor:    1.0000000000000000E-004
   Interstitial:
     Plane wave cutoff (in units of Gkmax):    1.0000000000000000     

--------------------------------------------------------------------------------

 Bare Coulomb potential parameters:
   Plane wave cutoff (in units of Gkmax*input%gw%MixBasis%gmb):    2.0000000000000000     
   Error tolerance for structure constants:    1.0000000000000001E-015
   Tolerance factor to reduce the MB size based on
   the eigenvectors of the bare Coulomb potential:   0.10000000000000001     

--------------------------------------------------------------------------------

 Screened Coulomb potential:
   Full-frequency Random-Phase Approximation

--------------------------------------------------------------------------------

 Core electrons treatment:
   xal - Core states are included in exchange but not in correlation

--------------------------------------------------------------------------------

 Interval of quasiparticle states (ibgw, nbgw):       1     10

 Number of empty states (GW):           60

 k/q-points grid:            7           7           7

--------------------------------------------------------------------------------
-                           Mixed product WF info                              -
--------------------------------------------------------------------------------

  Maximal number of MT wavefunctions per atom:          139
  Total number of MT wavefunctions:                     754
  Maximal number of PW wavefunctions:                   534
  Total number of mixed-product wavefunctions:         1288


--------------------------------------------------------------------------------
-                               frequency grid                                 -
--------------------------------------------------------------------------------

 Type: < fgrid > gauleg2                                 
 Frequency axis: < fconv > imfreq                                  
 Number of frequencies: < nomeg >           12
 Cutoff frequency: < freqmax >    1.0000000000000000     
 frequency list: < #    freqs    weight > 
   1  3.3765242898E-02  8.5662246190E-02
   2  0.1693953068      0.1803807865    
   3  0.3806904070      0.2339569673    
   4  0.6193095930      0.2339569673    
   5  0.8306046932      0.1803807865    
   6  0.9662347571      8.5662246190E-02
   7   1.034945175      9.1753818357E-02
   8   1.203942150      0.2614577472    
   9   1.614701292      0.6099867039    
  10   2.626806407       1.614329254    
  11   5.903351274       6.286190369    
  12   29.61625370       75.13628211    

--------------------------------------------------------------------------------
-                       Kohn-Sham eigenstates summary                          -
--------------------------------------------------------------------------------

 Maximum number of LAPW states:                      546
 Minimal number of LAPW states:                      518
 Number of states used in GW:
     - total KS                                       69
     - occupied                                        8
     - unoccupied                                     60
     - dielectric function                            69
     - self energy                                    69
 Energy of the highest unoccupied state:        2.898449
 Number of valence electrons:                         16
 Number of valence electrons treated in GW:           16

--------------------------------------------------------------------------------
-                          Kohn-Sham band structure                            -
--------------------------------------------------------------------------------

 Fermi energy:     0.0000
 Energy range:    -0.8231    2.8984
 Band index of VBM:   8
 Band index of CBm:   9

 Direct BandGap (eV):                      5.6608
 at k      =    0.000   0.000   0.000 ik =     1

================================================================================
=                                  GW cycle                                    =
================================================================================

 (task_gw): q-point cycle, iq =            1
 (task_gw): q-point cycle, iq =            2
 (task_gw): q-point cycle, iq =            3
 (task_gw): q-point cycle, iq =            4
 (task_gw): q-point cycle, iq =            5

--------------------------------------------------------------------------------
-                            G0W0 band structure                               -
--------------------------------------------------------------------------------

 Fermi energy:    -0.0021
 Energy range:    -0.9210    0.4356
 Band index of VBM:   8
 Band index of CBm:   9

 Direct BandGap (eV):                      8.6763
 at k      =    0.000   0.000   0.000 ik =     1

================================================================================
=                          GW timing info (seconds)                            =
================================================================================

 Initialization                             :         9.75
     - init_scf                             :         7.06
     - init_kpt                             :         1.76
     - init_eval                            :         0.06
     - init_freq                            :         0.00
     - init_mb                              :         0.42
 Subroutines                                : 
     - calcpmat                             :         4.89
     - calcbarcmb                           :        43.63
     - BZ integration weights               :        64.02
     Dielectric function                    :      5490.26
     - head                                 :         0.12
     - wings                                :         0.00
     - body                                 :         0.00
     - inversion                            :         7.70
     WF products expansion                  :         2.84
     - diagsgi                              :         1.19
     - calcmpwipw                           :         1.65
     - calcmicm                             :         0.00
     - calcminc                             :         0.05
     - calcminm                             :         0.00
     Self-energy                            :      4225.25
     - calcselfx                            :       664.73
     - calcselfc                            :      3560.52
     - calcvxcnn                            :         1.42
     - input/output                         :         0.00
 _________________________________________________________
 Total                                      :     12135.68


from ase.io import *
import  numpy as np

name = 'ice-vii'
atoms_0 = read('%s.cif'%name)

atoms = atoms_0 * (2,2,2)
group_1 = [2,4,5,9]
group_2 = [3,6,7,8]

del_list = []

for i, atom in enumerate(atoms):
    if i < len(atoms_0)*2 and i%len(atoms_0) in [4,5,7,8]:
        del_list.append(i)
    elif i >= len(atoms_0)*2 and i < len(atoms_0)*4 and i%len(atoms_0) in [2,9,3,6]:
        del_list.append(i)
    elif i >= len(atoms_0)*4  and i < len(atoms_0)*6 and i%len(atoms_0) in [2,4,6,8]:
        del_list.append(i)
    elif i >= len(atoms_0)*6  and i < len(atoms_0)*8 and i%len(atoms_0) in [5,9,3,7]:
        del_list.append(i)

mask = np.array([i not in del_list for i in range(len(atoms))])
atoms = atoms[mask]
write('ice-vii_2x2x2.traj',atoms)

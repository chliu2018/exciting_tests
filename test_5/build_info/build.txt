# building exciting-code with gcc

# load a suitable module:
module load buildenv-gcc/2018a-eb

# place "make.inc" file from this folder under "build" 
build/make.inc

# compile 
make all

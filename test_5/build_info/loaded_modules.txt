
Currently Loaded Modules:
  1) mpprun/4.3.0                                 6) GCCcore/6.4.0        11) OpenMPI/.2.1.2                   (H)  16) buildenv-gcc/2018a-eb
  2) nsc/.1.1                             (H,S)   7) binutils/.2.28  (H)  12) OpenBLAS/.0.2.20                 (H)  17) Anaconda/2021.05-nsc1
  3) EasyBuild/4.3.3-nscf4a9479                   8) GCC/6.4.0-2.28       13) FFTW/.3.3.7                      (H)
  4) nsc-eb-scripts/1.3                           9) numactl/.2.0.11 (H)  14) ScaLAPACK/.2.0.2-OpenBLAS-0.2.20 (H)
  5) buildtool-easybuild/4.3.3-nscf4a9479        10) hwloc/.1.11.8   (H)  15) foss/2018a

  Where:
   H:  Hidden Module
   S:  Module is Sticky, requires --force to unload or purge

 


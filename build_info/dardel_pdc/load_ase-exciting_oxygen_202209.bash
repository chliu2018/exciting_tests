module add PrgEnv-intel/8.2.0
source /cfs/klemming/projects/snic/xsolas/chliu/anaconda3/etc/profile.d/conda.sh
conda activate ase-3.22.1

export OMP_NUM_THREADS=1
export EXCITINGROOT=/cfs/klemming/projects/snic/xsolas/chliu/exciting_oxygen/exciting
export EXCITINGTOOLS=$EXCITINGROOT/tools
export TIMEFORMAT="   Elapsed time = %0lR"
export WRITEMINMAX="1"
export PYTHONPATH=$PYTHONPATH:$EXCITINGTOOLS/stm
export PATH=$PATH:$EXCITINGTOOLS:$EXCITINGROOT/bin:$EXCITINGTOOLS/stm

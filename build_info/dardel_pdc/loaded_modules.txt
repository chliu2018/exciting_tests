
Currently Loaded Modules:
  1) craype-x86-rome         4) perftools-base/21.10.0                          7) systemdefault/1.0.0 (S)  10) cray-dsmml/0.2.2       13) PrgEnv-intel/8.2.0
  2) libfabric/1.11.0.4.67   5) xpmem/2.2.40-7.0.1.0_2.4__g1d7a24d.shasta       8) intel/2022.0.2           11) cray-mpich/8.1.15
  3) craype-network-ofi      6) snic-env/1.0.0                            (S)   9) craype/2.7.12            12) cray-libsci/21.08.1.2

  Where:
   S:  Module is Sticky, requires --force to unload or purge

 
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
  PrgEnv-intel: PrgEnv-intel/8.2.0
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

    This module can be loaded directly: module load PrgEnv-intel/8.2.0

    Help:
          The PrgEnv-intel modulefile loads the Intel Programming Environment, 
          which includes the Intel compiler suite. This modulefile defines the system
          paths and environment variables needed to build an application using
          Intel for supported Cray systems.    
        
          This module loads the following modules:
           - cray-dsmml
           - cray-mpich
           - cray-libsci
       
          NOTE: This list is defined in /etc/cray-pe.d/cray-pe-configuration.sh.
